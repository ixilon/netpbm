package de.ixilon.netpbm;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import org.junit.Test;

public class NetpbmHeaderTest
{

    @Test
    public void decodesPbmHeader() throws FileNotFoundException, IOException
    {
        try (RandomAccessFile source = new RandomAccessFile(resource("plain.pbm"), "r"))
        {
            NetpbmHeader header = new NetpbmHeader(source);

            assertEquals(NetpbmFormat.P4, header.getFormat());
            assertEquals(18, header.getHeight());
            assertEquals(24, header.getWidth());
        }
    }

    @Test
    public void decodesPgmHeader() throws FileNotFoundException, IOException
    {
        try (RandomAccessFile source = new RandomAccessFile(resource("plain-8.pgm"), "r"))
        {
            NetpbmHeader header = new NetpbmHeader(source);

            assertEquals(NetpbmFormat.P5, header.getFormat());
            assertEquals(18, header.getHeight());
            assertEquals(24, header.getWidth());
        }
    }

    @Test
    public void decodesPpmHeader() throws FileNotFoundException, IOException
    {
        try (RandomAccessFile source = new RandomAccessFile(resource("plain-8.ppm"), "r"))
        {
            NetpbmHeader header = new NetpbmHeader(source);

            assertEquals(NetpbmFormat.P6, header.getFormat());
            assertEquals(18, header.getHeight());
            assertEquals(24, header.getWidth());
        }
    }

    private File resource(String name)
    {
        return new File(getClass().getClassLoader().getResource(name).getFile());
    }

}
