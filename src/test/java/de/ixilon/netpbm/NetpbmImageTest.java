package de.ixilon.netpbm;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import org.junit.Test;

public class NetpbmImageTest
{

    @Test
    public void readsPbmImage() throws IOException
    {
        try (NetpbmImage image = createImage("plain.pbm"))
        {
            assertEquals(1, image.getSample(10, 4));
        }
    }

    @Test
    public void readsPgmImage() throws IOException
    {
        try (NetpbmImage image = createImage("plain-8.pgm"))
        {
            assertEquals(44, image.getSample(10, 4));
        }
    }

    @Test
    public void readsPpmImage() throws IOException
    {
        try (NetpbmImage image = createImage("plain-8.ppm"))
        {
            assertEquals(44, image.getSample(10, 4, 0));
            assertEquals(44, image.getSample(10, 4, 1));
            assertEquals(44, image.getSample(10, 4, 2));
        }
    }

    private NetpbmImage createImage(String name) throws IOException
    {
        return new NetpbmImage(new RandomAccessFile(resource(name), "r"));
    }

    private File resource(String name)
    {
        return new File(getClass().getClassLoader().getResource(name).getFile());
    }
}
