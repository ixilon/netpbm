package de.ixilon.netpbm;

import java.io.Closeable;
import java.io.IOException;
import java.io.RandomAccessFile;

public class NetpbmImage implements Closeable
{
    private final RandomAccessFile file;
    private final NetpbmHeader header;
    private final int size;

    public NetpbmImage(RandomAccessFile file) throws IOException
    {
        this.file = file;
        this.header = new NetpbmHeader(file);
        this.size = sizeof(header.getMaxValue());
    }

    private static int sizeof(int value)
    {
        return (value <= 255) ? 1 : (value <= 65535) ? 2 : 4;
    }

    public int getSample(long x, long y) throws IOException
    {
        return getSample(x, y, 0);
    }

    public int getSample(long x, long y, int channel) throws IOException
    {
        file.seek(getOffset(x, y, channel));

        if (header.getFormat() == NetpbmFormat.P4)
        {
            byte b = file.readByte();
            long bit = x % 8;
            return (b >> bit) & 1;
        }

        switch (size)
        {
            case 1:
                return file.readUnsignedByte();
            case 2:
                return file.readUnsignedShort();
            case 4:
                return file.readInt();
        }

        throw new IOException("unsupported element size");
    }

    private long getOffset(long x, long y, int channel)
    {
        long offset = x + y * header.getWidth();

        switch (header.getFormat())
        {
            case P4:
                offset /= 8;
                break;
            case P5:
                offset *= size;
                break;
            case P6:
                offset *= 3 * size;
                offset += channel * size;
                break;
        }

        return offset + header.getOffset();
    }

    public long getWidth()
    {
        return header.getWidth();
    }

    public long getHeight()
    {
        return header.getHeight();
    }

    @Override
    public void close() throws IOException
    {
        file.close();
    }

}
