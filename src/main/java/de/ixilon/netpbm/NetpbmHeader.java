package de.ixilon.netpbm;

import java.io.IOException;
import java.io.RandomAccessFile;

class NetpbmHeader
{
    private final NetpbmFormat format;
    private final long width;
    private final long height;
    private final int maxValue;
    private final long offset;

    public NetpbmHeader(RandomAccessFile source) throws IOException
    {
        format = NetpbmFormat.valueOf(readString(source));
        width = Long.parseLong(readString(source));
        height = Long.parseLong(readString(source));
        maxValue = isMaxValue(format) ? Integer.parseInt(readString(source)) : 0;
        offset = source.getFilePointer();
    }

    private static boolean isMaxValue(NetpbmFormat format)
    {
        return format != NetpbmFormat.P4;
    }

    private static String readString(RandomAccessFile source) throws IOException
    {
        String line = source.readLine();

        if (line == null)
        {
            throw new IOException("unexpected end of file");
        }

        if (line.isEmpty() || line.startsWith("#")) {
            return readString(source);
        }

        return splitFirst(line, source);
    }

    private static String splitFirst(String line, RandomAccessFile source) throws IOException
    {
        String[] words = line.split("\\s", 2);

        if (words.length > 1)
        {
            source.seek(source.getFilePointer() - words[1].length() - 1);
        }

        return words[0];
    }

    public NetpbmFormat getFormat()
    {
        return format;
    }

    public long getWidth()
    {
        return width;
    }

    public long getHeight()
    {
        return height;
    }

    public int getMaxValue()
    {
        return maxValue;
    }

    public long getOffset()
    {
        return offset;
    }

}
