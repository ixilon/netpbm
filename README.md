# Netpbm #

[![build status](https://gitlab.com/ixilon/netpbm/badges/master/build.svg)](https://gitlab.com/ixilon/netpbm/commits/master)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/de.ixilon/netpbm/badge.svg)](https://maven-badges.herokuapp.com/maven-central/de.ixilon/netpbm)

## Overview ##
This is a Java library for reading and writing [Netpbm](http://netpbm.sourceforge.net/doc) files.

Files will not be buffered into RAM, because the intention of this library is to process 2-dimensional data which doesn't fit into memory.

## Usage ##
```
import de.ixilon.netpbm.NetpbmImage;

RandomAccessFile file = new RandomAccessFile(...);
NetpbmImage image = new NetpbmImage(file);

long x = 10;
long y = 20;
int sample = image.getSample(x, y);
```

## Restrictions ##
Only binary Netpbm files are supported.

## Supported Formats ##
* [PBM](http://netpbm.sourceforge.net/doc/pbm.html)
* [PGM](http://netpbm.sourceforge.net/doc/pgm.html)
* [PNM](http://netpbm.sourceforge.net/doc/pnm.html)